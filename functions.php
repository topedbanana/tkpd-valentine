<?php require_once('inc/backendframeworks.php');?>
<?php
//adding images
function images($images){
	echo get_template_directory_uri().'/assets/images/'.$images;
}

$framework=new backendframework();

$framework->addmenu=array(
		'top'    => __( 'Top Menu', 'sellertheme' ),
		'social' => __( 'Social Links Menu', 'sellertheme' ),
);
//adding font
$framework->addfont('Roboto+Opensans','https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|Roboto');
$framework->addfont('DancingScripts','https://fonts.googleapis.com/css?family=Dancing+Script');
//adding Bootstrap
$framework->addcss('bootstrapcss','bootstrap.min.css');
$framework->addcss('slick','slick.css');
$framework->addcss('fancyboxcss','jquery.fancybox.css');
$framework->addcss('slicktheme','slick-theme.css');
$framework->addjs('bootstrapjs','bootstrap.min.js');
$framework->addjs('slickjs','slick.min.js');
$framework->addjs('fancyboxjs','jquery.fancybox.js');
$framework->addjs('fancyboxjspack','jquery.fancybox.pack.js');
$framework->addjs('menucontroller','menucontroller.js');
$framework->addjs('slidercontroller','slidercontroller.js');
$framework->addjs('scrollto','scrolljs.js');
$framework->addjs('video','videocontroller.js');
add_image_size('toppicks','320','278',true);
add_image_size('instagram','420','280',true);
//adding general css
$framework->addgeneralcss();
$framework->addcss('mediaquery','mediaqueries.css');
$instagram=new oo_post_type('instagram','Instagram','Instagram','dashicons-info');
$toppicks=new oo_post_type('toppicks','Toppicks','Toppicks','dashicons-info');
//$framework->addcss('mediaqueries','mediaqueries.css');
//adding logo uploader
$addlogocustomizer=new addimagecustomizer('logo','logo','Logo Navigasi','Upload Logo Navigasi');
$addbannercustomizer=new addimagecustomizer('banner','banner','Banner Background','Banner Background');
$addbannercustomizer=new addimagecustomizer('bannerlogo','bannerlogo','Banner Logo Background','Banner Logo Background');
$addbannercustomizer=new addimagecustomizer('playvideo','playvideo','Play Video','Play Video');
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

// Defer Scrips
add_filter( 'clean_url', function( $url )
{
    if ( FALSE === strpos( $url, '.js' ) )
    { // not our file
        return $url;
    }
    // Must be a ', not "!
    return "$url' defer='defer";
}, 11, 1 );

// Disable W3TC footer comment for everyone but Admins (single site & network mode)
if ( !current_user_can( 'activate_plugins' ) ) {
    add_filter( 'w3tc_can_print_comment', '__return_false', 10, 1 );
}